jQuery(document).ready(function () {
    // ajustaPosicaoFormBanner();
    //
    // jQuery(window).resize(function () {
    //     ajustaPosicaoFormBanner();
    // });

    jQuery(".page-id-71").find("#box-texto").children("div").next().attr("id", "box-facasuacotacao");
    jQuery(".page-id-71").find("#box-texto").children("div").next().attr("class", "padrao1");

    jQuery(".page-id-303").find("#box-texto").children("div").next().attr("id", "box-facasuacotacao");
    jQuery(".page-id-303").find("#box-texto").children("div").next().attr("class", "padrao1");
    jQuery("#gal1").addClass("active");
    jQuery("#abas li").click(function () {
        var id = jQuery(this).attr("id");
        jQuery(".cont-abas").hide();
        jQuery("." + id).show();
        jQuery("#abas li").removeClass("active");
        jQuery(this).addClass("active");
    });
    jQuery(".box-mapa iframe").parent().addClass("box_mapa_mask");
    jQuery(".box-mapa iframe").addClass("mapa inative");
    jQuery(".box-mapa iframe").after("<div class='mask_mapa active'></div>");
    width_mask_mapa();

    jQuery(".box-mapa .mask_mapa").on('click', function () {
        jQuery(".box-mapa .mask_mapa").removeClass("active");
        jQuery(".box-mapa .mask_mapa").addClass("inative");

        jQuery(".box-mapa iframe").removeClass("inative");
        jQuery(".box-mapa iframe").addClass("active");
    });
    jQuery(".box-mapa iframe").mouseleave(function () {
        jQuery(".box-mapa .mask_mapa").removeClass("inative");
        jQuery(".box-mapa .mask_mapa").addClass("active");

        jQuery(".box-mapa iframe").removeClass("active");
        jQuery(".box-mapa iframe").addClass("inative");
    });
    jQuery(window).resize(function () {
        width_mask_mapa();
    });

    jQuery("#menu-principal").children("li").mouseover(function () {
        var n = jQuery(this).find(".sub-menu").length;
        if (n == 1) {
            jQuery(this).find(".sub-menu").show();
        }
    });

    jQuery("#menu-principal").children("li").mouseout(function () {
        var n = jQuery(this).find(".sub-menu").length;
        if (n == 1) {
            jQuery(this).find(".sub-menu").hide();
        }
    });
});

function width_mask_mapa() {
    var width_mapa = jQuery(".box-mapa iframe").width();
    var height_mapa = jQuery(".box-mapa iframe").height();
    jQuery(".box-mapa .mask_mapa.active").css("width", width_mapa);
    jQuery(".box-mapa .mask_mapa.active").css("height", height_mapa);
}

// function ajustaPosicaoFormBanner() {
//     var widthDaJanela = jQuery(window).width() - 17;
//
//     if (widthDaJanela >= 1120) {
//         var widthDoForm = (widthDaJanela - 1100) / 2 + 838 + 8;
//         jQuery("#box-facasuacotacao").css("margin-left", widthDoForm);
//     } else {
//         var widthDoForm = widthDaJanela - 262;
//         jQuery("#box-facasuacotacao").css("margin-left", widthDoForm);
//     }
// }
